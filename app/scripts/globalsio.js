/**
 * Created by ran on 6/21/15.
 */
(function (exports, firebaseApp, firebase, ajax, extend, TokenGenerator) {

    var gio = exports;
    gio.version = '0.0.1';

    if(!firebase)throw new Error('firebase dependency is missing');
    var FIREBASE_URL = 'https://' + firebaseApp + '.firebaseio.com';
    var firebaseRef = new firebase(FIREBASE_URL);
    var DATA_PATH = 'data';
    var dataRef = firebaseRef.child(DATA_PATH);


    var globalsKeyToPath = function(key){
        var arr = key.split('.');
        return arr.join('_');
    };

    var globalsPathToKey = function(path){
        var arr = path.split('_');
        return arr.join('.');
    };

    var authHandler = function(error, authData, cb){
        if(error){
            console.log('Login Failed!', error);
        } else {
            console.log('Login Success!', authData);
        }
        if(cb) cb(!!error ? error : authData);
    };

    var isNewUser = true;

    firebaseRef.onAuth(function(authData) {
        if (authData && isNewUser) {
            // save the user's profile into Firebase so we can list users,
            // use them in Security and Firebase Rules, and show profiles
            firebaseRef.child("users").child(authData.uid).set({
                provider: authData.provider,
                name: getName(authData)
            });
        }
    });
// find a suitable name based on the meta info given by each provider
    function getName(authData) {
        switch(authData.provider) {
            case 'password':
                return authData.password.email.replace(/@.*/, '');
            case 'twitter':
                return authData.twitter.displayName;
            case 'facebook':
                return authData.facebook.displayName;
            default :
                return authData.uid;
        }
    }

    var getAuthCallback = function(cb) {
        return function (error, authData) {
            authHandler(error, authData, cb)
        };
    };

    gio.login = function(email, password, cb) {
        var authHandleTmp = getAuthCallback(cb);

        firebaseRef.authWithPassword({
            email : email,
            password : password
        }, authHandleTmp)
    };

    gio.usernameLogin = gio.login;

    gio.customLogin = function(uid, secret, cb) {
        if(!TokenGenerator) throw new Error("TokenGenerator is required");
        var generator = new TokenGenerator(secret);
        var token = generator.createToken({uid: uid, admin: true});
        var authHandleTmp = getAuthCallback(cb);
        firebaseRef.authWithCustomToken(token, authHandleTmp);
    };

    gio.logout = function() {
        firebaseRef.unauth();
    };

    gio.authStatus = function() {
        return firebaseRef.getAuth();
    };

    function readGlobal(snapshot) {
        var global = snapshot.val();
        if(global._meta.uid){
            if(users[global._meta.uid]){
                global._meta.username = users[global._meta.uid].name;
            } else {
                console.log('cant find uid in users', global._meta.uid, users);
                global._meta.username = global._meta.uid;
            }
        }
        return  global;
    }

    gio.readValueOnce = function(key ,cb){
        var path = globalsKeyToPath(key);
        dataRef.child(path).once('value', function (snapshot) {
            cb(readGlobal(snapshot));
        });
    };

    gio.register = function(key, cb){
        var path = globalsKeyToPath(key);
        dataRef.child(path).on('value', function(snapshot){
            cb(readGlobal(snapshot))
        })
    };

    gio.unregister = function(key){
        var path = globalsKeyToPath(key);
        dataRef.child(path).off('value');
    };

    gio.writeValue = function(key, value, attributes, cb){
        var path = globalsKeyToPath(key);
        if(!firebaseRef.getAuth()){

            if(cb) {
                cb(new Error('not authenticated!'));
            }
        }
        var meta = {
            timestamp: firebase.ServerValue.TIMESTAMP,
            uid: firebaseRef.getAuth().uid
        };

        extend(meta, attributes);
        dataRef.child(path).set({
           _value :  value,
           _meta : meta
        },cb);
    };

    /**
     * the internals is intended to be used by globals.io internal services only, not by globals.io users
     * @type {{}}
     */
    gio.internals = {
        get: function (callback) {
          dataRef.once('value', function (snapshot) {
              var globalsJson = snapshot.val();
              Object.keys(globalsJson).map(function(global, index){
                  globalsJson[globalsPathToKey(global)] = globalsJson[global];
                  delete globalsJson[global];
              });
              callback(globalsJson);
          });
        },
        addUpdate: function(type, global, value, uid){
            firebaseRef.child('updates').push({
                type: type,
                global : global,
                value: value,
                uid: uid
            })
        },

        handleUpdates : function(handler) {
            var updatesRef = firebaseRef.child('updates');

            if(!handler) {
                updatesRef.off('child_added');
                return;
            }

            updatesRef.on('child_added', function (childSnap, prevChildKey) {
                var update = childSnap.val();
                console.log('new update task', update);
                if (!childSnap) return;
                var childSnapRef = childSnap.ref();
                childSnapRef.remove(function (err) {
                    if (!err) {
                        try {
                            handler(update);
                        } catch (error) {
                            console.log('updateData failed', error)
                        }
                    } else {
                        console.log('failed to remove update, not updating:', update, err);
                    }
                })
            });
        },

        addScrapper : function(global, url, selector, title, cb){
            var uid = gio.authStatus() ? gio.authStatus().uid : null;
            if(!uid) {
                var error = new Error('not authecticated, cant add scrappers)');
                if (cb) {
                    cb(error);
                    return;
                } else {
                    throw error;
                }
            }

            gio.writeValue(global, null, {title: title, sourceType: 'scrapper' ,source: url}, cb);

            var scrapper = {
                global : global,
                url : url,
                selector: selector,
                title : title,
                uid : uid
            };

            firebaseRef.child('scrappers').child(globalsKeyToPath(global)).set(scrapper, cb);
        },

        handleScrappers : function(handler){
            var scrappersRef = firebaseRef.child('scrappers');

            if(!handler) {
                scrappersRef.off('value');
            } else {
                scrappersRef.on('value', function(snap){
                    var scrappers = snap.val();
                    handler(scrappers);
                })
            }
        },

        handleUsers: function(handler){
            var usersRef = firebaseRef.child('users');
            if(!handler){
                usersRef.off('value')
            } else {
                usersRef.on('value',function(snap){ handler(snap.val())});
            }
        }
    };

    gio.util = {
        getQueryParams : function(){
            var query = {}, hash;
            var q = document.URL.split('?')[1];
            if(q != undefined){
                q = q.split('&');
                for(var i = 0; i < q.length; i++){
                    hash = q[i].split('=');
                    query[hash[0]] = hash[1];
                }
            }

            return query;
        }
    };

    var users = {};
    gio.internals.handleUsers(function(usersData){
        users = usersData;
    });


}(typeof exports === 'undefined'? this['gio']={}: exports,
    'globals',
        typeof exports === 'undefined' ? Firebase : require('firebase'),
        typeof exports === 'undefined' ? $.ajax : require('najax'),
        typeof exports === 'undefined' ? $.extend : require('extend'),
        typeof exports === 'undefined' ? null : require('firebase-token-generator')));
